package main

import (
	"flag"
	"fmt"

	"gitlab.com/hatricker/randpass/randpasswd"
)

func main() {

	var (
		lower  = flag.Int("lower", 8, "number of lower case letters")
		upper  = flag.Int("upper", 8, "number of upper case letters")
		nn     = flag.Int("number", 3, "number of numbers")
		symbol = flag.Int("sym", 3, "number of symbols")
	)

	flag.Parse()

	lens := [4]int{*lower, *upper, *nn, *symbol}

	fmt.Println(randpasswd.GenerateRandomPassword(lens))
}
