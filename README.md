randpasswd is a random password generator

# Build and Install
```
make install
```

# Run tests
```
make test
```

# Usage
```
randpasswd
```

You can also check the option flags to change the length of password