GOPATH ?= $(HOME)/go
GOBIN=$(GOPATH)/bin

.PHONY: install
install:
	GOBIN=$(GOBIN) go install cmd/randpasswd.go

.PHONY: test
test:
	go test -cover ./...

.PHONY: fmt
fmt:
	go fmt ./...

