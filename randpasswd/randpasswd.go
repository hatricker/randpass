package randpasswd

import (
	"math/rand"
	"time"
)

var (
	charMap = map[int]string{
		0: "abcdefghijklmnopqrstuvwxyz",
		1: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
		2: "0123456789",
		3: "!@#$%&*",
	}
	randGen = &randNumberGen{}
)

type numberGen interface {
	generateNumber(int) int
}

type randNumberGen struct {
}

func (rng randNumberGen) generateNumber(upper int) int {
	return rand.Intn(upper)
}

//GeneratePassword generates the password
//lengthComb defines how many characters are needed for each of the category
//gen is a number generator
func GeneratePassword(lengthComb [4]int, gen numberGen) string {
	totalLength := lengthComb[0] + lengthComb[1] + lengthComb[2] + lengthComb[3]
	if totalLength == 0 {
		return ""
	}

	passwords := make([]byte, 0, totalLength)

	for i := 0; i < len(lengthComb); i++ {
		length := len(charMap[i])
		for j := 0; j < lengthComb[i]; j++ {
			index := gen.generateNumber(length)
			passwords = append(passwords, charMap[i][index])
		}
	}

	return string(passwords)
}

//GenerateRandomPassword generates random password
func GenerateRandomPassword(lengthComb [4]int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	return GeneratePassword(lengthComb, randGen)
}
