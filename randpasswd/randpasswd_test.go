package randpasswd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type testNumberGen struct {
	index int
}

func (tng testNumberGen) generateNumber(_ int) int {
	return tng.index % 7
}

func TestGeneratePassword(t *testing.T) {
	assert := assert.New(t)

	tests := []struct {
		input  [4]int
		index  int
		result string
	}{
		{
			[4]int{1, 1, 1, 1},
			0,
			"aA0!",
		},
		{
			[4]int{2, 3, 1, 1},
			1,
			"bbBBB1@",
		},
		{
			[4]int{},
			1,
			"",
		},
	}
	for _, tt := range tests {
		testGen := &testNumberGen{index: tt.index}
		result := GeneratePassword(tt.input, testGen)
		assert.Equal(tt.result, result)
	}
}
